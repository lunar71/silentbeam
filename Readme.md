SilentBeam
==========

#### About
SilentBeam is a bash-only project designed to turn a default Raspbian/Raspberry Pi OS installation into a moderately stealthy network implant.
It configures the Raspberry Pi with an AutoSSH service to connect to a C2 of your choice. It also configures a MAC and hostname randomization service on boot (e.g. Microsoft vendor MAC address + DESKTOP-MFKR55A hostname)

#### What it actually configures
- installs autossh vim git tmux screen tcpdump libssl-dev macchanger ufw mlocate nmap masscan python libffi6 freetds-dev curl libffi-dev python-pip python3 python3-pip virtualenv default-mysql-client postgresql-client ruby ruby-dev
- installs the following python2 packages via pip: impacket requests pwntools BeautifulSoup4 colorama pysnmp mssql-cli 'scapy>=2.3.1' 'wsgiref>=0.1.2'
- installs the following python3 packages via pip: impacket requests pwntools BeautifulSoup4 colorama pysnmp mssql-cli 'scapy>=2.3.1'
- installs the following ruby gems: winrm winrm-fs stringio
- adds a new user account "silentbeam" and deletes the default "pi" user account
- configures the AutoSSH service with user input regarding the C2 server
- generates an SSH keypair for AutoSSH use
- mounts /tmp and /var/log to tmpfs to decrease SD card writes
- configures the MAC and hostname randomization service
- removes /etc/issue, /etc/motd
- disables IPv6
- configures various services
- configures the Raspberry Pi with raspi-config noninteractively, with the following values: do_serial 1, do_expand_rootfs, do_boot_splash 1, do_camera 0, do_spi 1, do_memory_split 64, do_i2c 1, do_onewrite 1, do_gldriver G3, do_rgpio 0, do_wifi_country US, do_change_timezone Etc/UTC
- installs latest golang version

#### How to turn your Raspberry Pi into SilentBeam
Install Raspbian/Raspberry Pi OS on an SD card, enable SSH by creating the "ssh" file on the /boot partition, clone this repo and run silentbeam.sh

#### TODO
- add more MAC vendors + hostnames
- more tools (impacket, mssql, evil-winrm, aquatone, ffuf/gobuster, nmap, bettercap etc)
- implement for other SBCs (e.g. Orange Pi)
